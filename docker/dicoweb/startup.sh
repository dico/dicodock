#!/bin/sh
python manage.py collectstatic --no-input
chgrp -R users /app/static/ && chmod -R g+w /app/static
