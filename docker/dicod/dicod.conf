# Configuration file for docker-based installations of GNU Dico.
# Copyright (C) 2023 Sergey Poznyakoff
#
# GNU Dico is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Dico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Dico.  If not, see <http://www.gnu.org/licenses/>.

capability (${DICOD_CAPABILITY:-mime,markup,xversion,xlev});

user nobody;
max-children ${DICOD_MAX_CHILDREN:-16};
inactivity-timeout ${DICOD_INACTIVITY_TIMEOUT:-10};

log-facility ${DICOD_LOG_FACILITY:-daemon};
transcript no;
pidfile "/var/run/dico/dicod.pid";

$$ifdef DICOD_ACCESS_LOG_FILE
access-log-format "%h %l %u %t \"%r\" %>s %b \"-\" \"%C\" %D";
access-log-file "/var/log/dico/$DICOD_ACCESS_LOG_FILE";
identity-check no;
$$endif

server-info <<EOT
No further information available.
EOT;

acl global {
	allow all from (127.0.0.1${DICOD_SYSINFO_ACL:+, $DICOD_SYSINFO_ACL});
	deny all;
}

show-sys-info global;
timing yes;

$$ifdef DICOD_DECLARE_ALIASES
alias d DEFINE;
alias da d "*";
alias df d "!";
alias m MATCH;
alias mas m "*";
alias mfs m "!";
alias ma mas ".";
alias mf mfs ".";
alias s STATUS;
alias h HELP;
alias q QUIT;

help-text <<- EOT
        +
  The following commands are abbreviations designed to facilitate manual
  interaction with the daemon.  Do not rely on them when writing client
  software.  They may disappear or change without notice.

  d database word                 -- DEFINE database word
  da word                         -- DEFINE * word
  df word                         -- DEFINE ! word
  ma word                         -- MATCH * . word
  mf word                         -- MATCH ! . word
  mas strategy word               -- MATCH * strategy word
  mfs strategy word               -- MATCH ! strategy word
  m database strategy word        -- MATCH database strategy word
  s                               -- STATUS
  h                               -- HELP
  q                               -- QUIT
EOT;
$$endif

${* Search for freedict dictionary and build the corresponding configuration. *}
$$set FREEDICT_DIR "/var/dicodb/freedict"
$$ifcom test -d $FREEDICT_DIR
$$set dblist "$(find $FREEDICT_DIR -type f -name '*.index' -printf '%P\n' | sed -e 's/\.index$//')"
$$ifset dblist
load-module dictorg {
        command "dictorg trim-ws dbdir=$FREEDICT_DIR";
}

$$loop dbname $dblist
$$ifcom test -f $FREEDICT_DIR/$dbname.dict.dz
database {
        name "$(basename $dbname)";
        handler "dictorg database=$dbname";
}

$$endif
$$end
$$endif
$$endif

${* Test if GCIDE database is present. *}
$$set GCIDE_DIR "/var/dicodb/gcide"
$$ifcom test -d $GCIDE_DIR
$$ifcom test "$(find $GCIDE_DIR -maxdepth 1 -name 'CIDE.*' -printf '%f\n' | sed -e 's/CIDE\.//' | sort | tr -d '\n')" = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
load-module gcide;
database {
        name "gcide";
        handler "gcide dbdir=$GCIDE_DIR";
}
$$else
$$loop subdir $(find $GCIDE_DIR -mindepth 1 -maxdepth 1 -type d)
$$ifcom test "$(find $subdir -maxdepth 1 -name 'CIDE.*' -printf '%f\n' | sed -e 's/CIDE\.//' | sort | tr -d '\n')" = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
load-module gcide;
database {
        name "gcide";
        handler "gcide dbdir=$subdir";
}

$$endif
$$end
$$endif
$$endif

${* Test if WordNet database is present. *}
$$set WORDNET_DIR "/var/dicodb/wordnet"
$$ifcom test -d $WORDNET_DIR
$$ifcom test "$(find $WORDNET_DIR -maxdepth 1 -name 'index.*' -printf '%P\n' | sed -e 's/index\.//'| sort | tr '\n' ',')" = "adj,adv,noun,sense,verb,"
load-module wordnet {
        command "wordnet wnsearchdir=$WORDNET_DIR";
}
database {
        name "WordNet";
        handler "wordnet merge-defs";
        description "WordNet dictionary";
}
$$else
$$loop subdir $(find $WORDNET_DIR -mindepth 1 -maxdepth 1 -type d)
$$ifcom test "$(find $subdir -maxdepth 1 -name 'index.*' -printf '%P\n' | sed -e 's/index\.//'| sort | tr '\n' ',')" = "adj,adv,noun,sense,verb,"
load-module wordnet {
        command "wordnet wnsearchdir=$subdir";
}
database {
        name "WordNet";
        handler "wordnet merge-defs";
        description "WordNet dictionary";
}

$$endif
$$end
$$endif
$$endif

#include "/etc/dicod/include/*.conf"
