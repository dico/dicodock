# This file is part of dicodock - a containerized environment for GNU Dico.
# Copyright (C) 2024 Sergey Poznyakoff
#
# Dicodock is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Dicodock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dicodock.  If not, see <http://www.gnu.org/licenses/>.

PROJECT = dicodock
command = docker compose -p $(PROJECT)
ifneq (,$(wildcard config.mk))
  include config.mk
endif
ifeq (,$(wildcard .env))
  $(warning No .env file found. Using built-in defaults.)
  export DICOD_INITDB := all
  PROFILE = nginx
else
  command += --env-file .env
  include .env
endif
ifdef ANSI
  command += --ansi=$(ANSI)
  ifeq ($(ANSI),never)
    export DOCKER_BUILDKIT := 0
    ifneq (,$(shell docker compose --help|grep -- --progress))
      command += --progress=plain
    endif
  endif
endif
ifneq (,$(PROFILE))
  command += --profile $(PROFILE)
endif

F=
up_FLAGS = -d

define goal =
$(1):
	@$$(command) $(1) $$($(strip $(1))_FLAGS)$(if $(F), $(F))
endef

define impgoal =
$(1):
	@$$(command) $(subst -%,,$(1)) \
                     $$($(strip $(subst -%,,$(1)))_FLAGS)$(if $(F), $(F)) $$*
endef

all: build
build: bootstrap

$(foreach verb, build config up down restart ps logs, \
$(eval $(call goal, $(verb))))

$(foreach verb, start-% stop-% restart-% ps-% logs-%, \
$(eval $(call impgoal, $(verb))))

bootstrap:
	@git submodule update --init --recursive



